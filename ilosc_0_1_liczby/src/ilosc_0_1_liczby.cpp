//============================================================================
// Name        : ilosc_0_1_liczby.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {
	cout << "Program zlicza ilosc zer i jedynek w liczbie" << endl;
	int liczba;
	cout << "Wprowadz liczbe: ";
	cin >> liczba;
	unsigned short licznik0(0);
	unsigned short licznik1(0);
	if (liczba==0) // petla nie wykona sie dla liczby zero stad warunek
		licznik0 = 1;
	while (liczba > 0) {
		if (liczba & 1)
			++licznik1;
		else
			++licznik0;
		liczba = liczba >> 1;
	}
	cout << "Liczba zer (0) w zapisie binarnym podanej liczby wynosi: " << licznik0
			<< endl;
	cout << "Liczba jedynek (1) w zapisie binarnym podanej liczby wynosi: "
			<< licznik1 << endl;
	return 0;
}
