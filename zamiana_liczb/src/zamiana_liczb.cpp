//============================================================================
// Name        : zamiana_liczb.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {
	cout << "Program zamienia liczby przy uzyciu operacji bitowych" << endl;
	int liczba1;
	int liczba2;
	cout << "Wprowadz pierwsza liczbe: ";
	cin >> liczba1;
	cout << "Wprowadz druga liczbe: ";
	cin >> liczba2;
	cout << "Zamieniam..." << endl;
	liczba1 = liczba1 ^ liczba2;
	liczba2 = liczba1 ^ liczba2;
	liczba1 = liczba1 ^ liczba2;
	cout << "Liczba pierwsza: " << liczba1 << endl;
	cout << "Liczba druga: " << liczba2 << endl;
	return 0;
}
