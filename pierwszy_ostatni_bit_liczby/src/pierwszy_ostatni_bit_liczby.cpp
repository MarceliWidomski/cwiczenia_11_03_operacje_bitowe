//============================================================================
// Name        : pierwszy_ostatni_bit_liczby.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {
	cout << "Program sprawdza jaki jest pierwszy i ostatni bit liczby" << endl; // czy pierwszy bit nie jest zawsze ustawiony?
	cout << "Wprowadz liczbe: ";
	int liczba;
	cin >> liczba;
	if ((liczba & 1) == 0)
		cout << "Ostatni bit jest nieustawiony (0)" << endl;
	else
		cout << "Ostatni bit jest ustawiony (1)" << endl;
	while (liczba > 1)
		liczba = liczba >> 1;
	if ((liczba & 1) == 0)
		cout << "Pierwszy bit jest nieustawiony (0)" << endl;
	else
		cout << "Pierwszy bit jest ustawiony (1)" << endl;
	return 0;
}
