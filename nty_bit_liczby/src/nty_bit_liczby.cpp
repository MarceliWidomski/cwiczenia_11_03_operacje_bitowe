//============================================================================
// Name        : nty_bit_liczby.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {
	cout << "Program wypisuje n-ty bit liczby" << endl;
	int liczba;
	short nrBitu(-1);
	while (nrBitu<0){
	cout << "Wprowadz liczbe: ";
	cin >> liczba;
	cout << "Wprowadz numer bitu ktory chcesz sprawdzic: "; //bity numerowane od 0
	cin >> nrBitu;
	if (nrBitu<0)
		cout << "Numer bitu niewlasciwy, sprobuj ponownie. " << endl;
	}
	int maska=1;
	maska=maska<<nrBitu;
	if (liczba&maska) // sprawdza czy n-ty bit jest ustawiony
		cout << "Bit nr " << nrBitu << " jest ustawiony (1)"<< endl;
	else
		cout << "Bit nr " << nrBitu << " jest nieustawiony (0)"<< endl;
	return 0;
}
