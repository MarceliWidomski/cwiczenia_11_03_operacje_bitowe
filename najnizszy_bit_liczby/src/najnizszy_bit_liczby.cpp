//============================================================================
// Name        : najnizszy_bit_liczby.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {
	cout << "Program wypisuje numer najnizszego ustawionego bitu w liczbie" << endl;
	int liczba;
	cout << "Wprowadz liczbe: ";
	cin >> liczba;
	unsigned short licznik(0);
	while ((liczba & 1)==0) {
		liczba = liczba >> 1;
		++licznik;
	}
	cout << "Najnizszy ustawiony bit liczby ma numer: " << licznik << endl; // liczy od 0
	return 0;
}
