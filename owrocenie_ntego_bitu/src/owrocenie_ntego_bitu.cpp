//============================================================================
// Name        : owrocenie_ntego_bitu.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {
	cout << "Program odwraca n-ty bit" << endl; //2 sposob
	int liczba;
	int nrBitu(-1);
	while (nrBitu < 0) {
		cout << "Wprowadz liczbe: ";
		cin >> liczba;
		cout << "Wprowadz numer bitu ktory chcesz odwrocic: "; //bity numerowane od 0
		cin >> nrBitu;

		if (nrBitu < 0)
			cout << "Numer bitu niewlasciwy, sprobuj ponownie. " << endl;
	}
	int maska(1);
	maska = maska<<nrBitu;
	liczba = liczba ^ maska;
	cout << "Liczba po odwroceniu " << nrBitu << "-go bitu wynosi " << liczba
			<< endl;
	return 0;
}
