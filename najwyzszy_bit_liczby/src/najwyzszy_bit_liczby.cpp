//============================================================================
// Name        : najwyzszy_bit_liczby.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {
	cout << "Program wypisuje numer najwyzszego ustawionego bitu w liczbie" << endl;
	int liczba;
	cout << "Wprowadz liczbe: ";
	cin >> liczba;
	unsigned short licznik(-1);
	while (liczba > 0) {
		liczba = liczba >> 1;
		++licznik;
	}
	cout << "Najwyzszy ustawiony bit liczby ma numer: " << licznik << endl; // liczy od 0
	return 0;
}
